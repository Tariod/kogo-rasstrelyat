import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import {connect} from 'react-redux';
import {fetchStudents, updateScore} from '../../actions/actions';
import './Table.css';

class TableComponent extends Component {
  componentDidMount() {
    this.props.fetchStudents()
  }

  render() {
    const { students } = this.props;

    const columns = [
      {
        Header: 'Номер зачетки',
        accessor: 'id',
        filterMethod: ((filter, row) =>
          String(row[filter.id]) === filter.value
        )
      },  {
        Header: 'Группа',
        accessor: 'group'
      }, {
        Header: 'Имя',
        accessor: 'name'
      }, {
        Header: 'Балл',
        accessor: 'score',
        Cell: (cellData) => (
          <div
            style={{ backgroundColor: "#fafafa" }}
            contentEditable
            suppressContentEditableWarning
            onBlur={e => {
              const val = e.target.innerHTML;
              this.props.updateScore(cellData.original.id, val);
            }}
            dangerouslySetInnerHTML={{
              __html: students[cellData.index][cellData.column.id]
            }}
          />
        )
      }, {
        Header: 'Вердикт',
        accessor: 'score',
        id: 'decision',
        Cell: ({ value }) => (value >= 60 ? "Оставить" : "Избавиться"),
        filterMethod: (filter, row) => {
          if (filter.value === "all") {
            return true;
          }
          if (filter.value === "true") {
            return row[filter.id] >= 60;
          }
            return row[filter.id] < 60;
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "all"}
          >
            <option value="all">Все</option>
            <option value="true">Помилованные</option>
            <option value="false">Репрессированные</option>
          </select>
      }
    ];

    return (
      <div className='bottom'>
        <ReactTable
          data={ students }
          filterable
          defaultFilterMethod={(filter, row) => 
            row[filter.id].toLowerCase().includes(filter.value.toLowerCase())
          }
          style={{
            height: "400px"
          }}
          columns={ columns }
          defaultPageSize={ 30 }
        />
<input/>
<input/>
<input/>
      </div>
    );
  }
}

export default connect(
  (state) => ({students: state.student.students}),
  {fetchStudents, updateScore}
)(TableComponent)

