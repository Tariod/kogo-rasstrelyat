import {getStudents, updateStudent} from '../api';
import {STUDENT_REPLACE, STUDENTS_LOAD} from '../reducers/students';

export const loadStudents = (students) => ({type: STUDENTS_LOAD, payload: students})
export const replaceStudent = (student) => ({type: STUDENT_REPLACE, payload: student })

export const fetchStudents = () => {
  return (dispatch) => {
    getStudents()
      .then(students => dispatch(loadStudents(students)))
  }
}

export const updateScore = (id, val) => {
  return (dispatch, getState) => {
    const {students} = getState().student;
    const student = students.find(s => s.id === id);
    const changed = {...student, score: val};
    updateStudent(changed)
      .then(res => dispatch(replaceStudent(res)))
  }
}

