import React, { Component } from "react";
import {
  Route,
  NavLink,
  BrowserRouter
} from "react-router-dom";
import Home from "../components/Home/HomeComponent";
import Table from "../components/Table/TableComponent";

class Main extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <ul className="header">
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/table">List</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route path="/table" component={Table}/>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}
 
export default Main;
