const initState = {
  students: []
}

export const STUDENT_REPLACE = 'STUDENT_REPLACE'
export const STUDENTS_LOAD = 'STUDENTS_LOAD'

export default (state = initState, action) => {
  switch (action.type) {
    case STUDENT_REPLACE:
      return {...state,
        students: state.students
          .map(s => s.id === action.payload.id ? action.payload : s)
      }
    case STUDENTS_LOAD:
      return {...state, students: action.payload}
    default:
      return state
  }
}

