export const getStudents = () => {
  return fetch('http://localhost:8080/students')
    .then(res => res.json())
}

export const updateStudent = (student) => {
  return fetch(`http://localhost:8080/students/${student.id}`, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(student)
  })
    .then(res => res.json())
}

